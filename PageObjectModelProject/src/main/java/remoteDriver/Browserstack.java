package remoteDriver;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class Browserstack {

	public WebDriver getDriver(String os, String os_ver, String browser) {
		String username = System.getenv("BROWSERSTACK_USERNAME");
		String accessKey = System.getenv("BROWSERSTACK_ACCESS_KEY");
		String browserstackLocal = System.getenv("BROWSERSTACK_LOCAL");
		String browserstackLocalIdentifier = System.getenv("BROWSERSTACK_LOCAL_IDENTIFIER");
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("os", os);
		capabilities.setCapability("os_version", os_ver);
		capabilities.setCapability("browser", browser);
		
		WebDriver driver = null;
		System.out.println("username"+username);
		System.out.println("accessKey"+accessKey);
		try {
			driver = new RemoteWebDriver(
					new URL("https://" + username + ":" + accessKey + "@hub.browserstack.com/wd/hub"), capabilities);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return driver;
	}
}
