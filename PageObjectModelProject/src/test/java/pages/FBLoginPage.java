package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class FBLoginPage {

	WebDriver driver;
	public static String errorMessage = "The email you’ve entered doesn’t match any account. Sign up for an account.";

	public FBLoginPage(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(how = How.XPATH, using = "//input[@type='text'][@name='email']")
	WebElement emailTextBox;
	@FindBy(how = How.XPATH, using = "//input[@type='password'][@name='pass']")
	WebElement passwordTextBox;
	@FindBy(how = How.XPATH, using = "//button[@type='submit'][@data-testid='royal_login_button']")
	WebElement signinButton;
	@FindBy(how = How.XPATH, using = "//div[@class='_9ay7']")
	WebElement errorText;

	public void setEmail(String strEmail) {
		emailTextBox.sendKeys(strEmail);
	}

	public void setPassword(String strPassword) {
		passwordTextBox.sendKeys(strPassword);
	}

	public void clickOnLoginButton() {
		signinButton.click();
	}

	public String getErrorMessage() {
		return errorText.getText();
	}
}