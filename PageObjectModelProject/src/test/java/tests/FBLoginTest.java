package tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import pages.*;

public class FBLoginTest extends TestBase {

	@Test
	public void validateLoginCredentials() throws Exception {

		FBLoginPage apage = PageFactory.initElements(driver, FBLoginPage.class);
		apage.setEmail("testuser@gmail.com");
		apage.setPassword("password");
		apage.clickOnLoginButton();
		apage.getErrorMessage().equalsIgnoreCase(
				"The email address or phone number that you've entered doesn't match any account. Sign up for an account.");

		// FBHomePage homepage = PageFactory.initElements(driver, FBHomePage.class);
		// homepage.clickOnProfileDropdown();
		// homepage.verifyLoggedInUserNameText();
		// homepage.clickOnLogoutLink();
	}

}