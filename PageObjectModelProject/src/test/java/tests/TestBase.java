package tests;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import remoteDriver.Browserstack;

public class TestBase {

	public static WebDriver driver = null;

	@BeforeSuite
	public void initialize(){
		System.out.println("Test Suite starts");
	}

	@Test (dataProvider = "listOfBrowser")
	public void testo(String os, String os_ver, String browser)  throws Exception {

		driver = new Browserstack().getDriver(os,os_ver,browser);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		String url = "https://mycutebaby.in/contest/participant/63c937a267008?utm_source=wsapp_share&utm_campaign=January_2023&utm_medium=shared&utm_term=wsapp_shared_63c937a267008&utm_content=participant";
		driver.get(url);
		driver.findElement(By.xpath("//input[@name=\"v\"]")).sendKeys("AK");
		driver.findElement(By.xpath("//a[@id=\"vote_btn\"]")).click();
		Thread.sleep(500);

		//validations

		String postclick = driver.findElement(By.xpath("//div[@class=\"vt_container\"]/h1[2]")).getText();
		//Assert.assertEquals(postclick.trim(),"Vote button will appear here after");
		String votecount = driver.findElement(By.xpath("//h3[@id=\"vote-count\"]")).getText();
		System.out.println(votecount);
		String rank = driver.findElement(By.xpath("//span[@class=\"ranksinfoshow\"]")).getText();
		System.out.println(rank);

		// Setting the status of test as 'passed' or 'failed' based on the condition; if title of the web page matches 'BrowserStack - Google Search'
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		if (postclick.equals("Vote button will appear here after")) {
			jse.executeScript("browserstack_executor: {\"action\": \"setSessionStatus\", \"arguments\": {\"status\": \"passed\", \"reason\": \"Vote completed!\"}}");
		}
		else {
			jse.executeScript("browserstack_executor: {\"action\": \"setSessionStatus\", \"arguments\": {\"status\":\"failed\", \"reason\": \"Vote failed\"}}");
			Assert.fail("Vote failed");
		}
	}

	@AfterSuite
	public void TeardownTest() {
		TestBase.driver.quit();
		System.out.println("Test Suite ends");
	}

	@DataProvider(name = "listOfBrowser")
	public Object[][] dpMethod(){
		return new Object[][] {
				{"Windows", "11", "Chrome"},
				{"Windows", "10", "Chrome"},
				{"Windows", "8.1", "Chrome"},
				{"Windows", "7", "Chrome"},
				{"OS X", "Ventura", "Chrome"},
				{"OS X", "Monterey", "Chrome"},
				{"OS X", "Big Sur", "Chrome"},
				{"OS X", "Catalina", "Chrome"},
				{"OS X", "Mojave", "Chrome"},
				{"OS X", "High Sierra", "Chrome"},
		};
	}

}